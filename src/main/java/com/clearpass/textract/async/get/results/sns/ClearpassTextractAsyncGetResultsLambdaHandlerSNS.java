package com.clearpass.textract.async.get.results.sns;

import java.time.Duration;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.client.builder.AwsClientBuilder.EndpointConfiguration;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.SNSEvent;
import com.amazonaws.services.sns.AmazonSNS;
import com.amazonaws.services.sns.AmazonSNSClientBuilder;
import com.amazonaws.services.textract.AmazonTextract;
import com.amazonaws.services.textract.AmazonTextractClientBuilder;
import com.amazonaws.services.textract.model.Block;
import com.amazonaws.services.textract.model.DocumentMetadata;
import com.amazonaws.services.textract.model.GetDocumentTextDetectionRequest;
import com.amazonaws.services.textract.model.GetDocumentTextDetectionResult;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.joestelmach.natty.DateGroup;
import com.joestelmach.natty.Parser;

/*
 * Author: Amar Reddy
 * Lambda function to handle image and detect text using aws textractClient SDK
 */
public class ClearpassTextractAsyncGetResultsLambdaHandlerSNS implements RequestHandler<SNSEvent, String> {
	private final String LOG2CLOUDWATCH = System.getenv("LOG2CLOUDWATCH");
	private final String ENDPOINTCONFIGURL = "https://textract.us-east-1.amazonaws.com";
	private final String ENDPOINTCONFIGURL_REGION = "us-east-1";
	// turn on/off the cloudwatch log entries-default is false. Gets updated
	// with environment variable LOG2CLOUDWATCH
	static boolean bLog2Cloudwatch = false;
	public static final int BUFFER_SIZE = 64 * 1024;
	private static String startJobId = null;
	private static AmazonTextract textractClient = null;
	static List<String> linesText = new ArrayList<>();
	final AmazonSNS sqs = AmazonSNSClientBuilder.standard().build();

	public enum ProcessType {
		DETECTION, ANALYSIS
	}

	@Override
	public String handleRequest(SNSEvent event, Context context) throws RuntimeException {
		Instant startInstant = Instant.now();
		LambdaLogger logger = context.getLogger();
		if (!createAWSServiceClients(logger)) {
			Log2Cloudwatch(logger, "Failed to create AWS SDK service objects");
		}
		// read the log config values
		bLog2Cloudwatch = LOG2CLOUDWATCH.equalsIgnoreCase("true") ? true : false;
		try {
			String jsonStr = event.getRecords().get(0).getSNS().getMessage();
			Log2Cloudwatch(logger, "SNS Message: " + jsonStr);
			try {
				Log2Cloudwatch(logger,
						"Time Before processing message: " + ZonedDateTime.now());
				processMessage(jsonStr, logger);
				Log2Cloudwatch(logger,
						"Time After processing: message: " + ZonedDateTime.now());
			} catch (Exception e) {
				Log2Cloudwatch(logger, e.getMessage());
				throw new RuntimeException("Putback messages in Queue");
			}
			return "Notification processed:";
		} 
		catch (final AmazonServiceException serviceException) {
			String msg1 = "Caught an AmazonServiceException, which means "
					+ "your request made it to Amazon Lambda, but was " + "rejected.";
			String msg2 = "Error Message:    " + serviceException.getMessage();
			String msg3 = "HTTP Status Code: " + serviceException.getStatusCode();
			String msg4 = "AWS Error Code:   " + serviceException.getErrorCode();
			String msg5 = "Error Type:       " + serviceException.getErrorType();
			String msg6 = "Request ID:       " + serviceException.getRequestId();

			String errorStr = String.format("%s\n %s\n %s\n %s\n %s\n  %s\n", msg1, msg2, msg3, msg4, msg5, msg6);
			logger.log(errorStr);
			throw new RuntimeException(errorStr);
		} catch (final AmazonClientException ace) {
			String errorStr = "AmazonClientException: Error Message: " + ace.getMessage();
			logger.log(errorStr);
			throw new RuntimeException(errorStr);
		} catch (Exception ex) {
			String errorStr = "Exception GetDocumentTextDetectionResults: Error Message: " + ex.getMessage();
			Log2Cloudwatch(logger,errorStr);
			throw new RuntimeException("Lambda failed to detect text from uploaded document");
		} finally {
			Instant endInstant = Instant.now();
			Duration duration = Duration.between(startInstant, endInstant);
			Log2Cloudwatch(logger, "Lambda Executed - end \n" + "Exceution Time(secs) = " + duration.getSeconds());
		}
	}
	
	private boolean createAWSServiceClients(LambdaLogger logger) {
		try {
			final EndpointConfiguration endpoint = new EndpointConfiguration(ENDPOINTCONFIGURL, ENDPOINTCONFIGURL_REGION);
			textractClient = AmazonTextractClientBuilder.standard().withEndpointConfiguration(endpoint).build();
		} catch (final AmazonClientException ace) {
			String errorStr = "AmazonClientException: Error Message: " + ace.getMessage();
			logger.log(errorStr);
			return false;
		}
		return true;
	}
	
	private void processMessage(String notification, LambdaLogger logger) {
		//create object from the sns message
		Gson gson = new Gson();
		JsonParser parser = new JsonParser();
		JsonObject object = (JsonObject) parser.parse(notification);
		Message snsMessageObject = gson.fromJson(object, Message.class); 
		if(snsMessageObject != null) {
			startJobId = snsMessageObject.getJobId();
			String jobStatus = snsMessageObject.getStatus();
			if(jobStatus.equals("SUCCEEDED")) {
				// Found job. Get the results and display
				Log2Cloudwatch(logger,"Job found was: " + startJobId);
				try {
					GetDocumentTextDetectionResults(startJobId, logger);
				} catch (Exception e) {
					Log2Cloudwatch(logger,"GetDocumentTextDetectionResults Falied " + startJobId);
				}
			}
			else {
				Log2Cloudwatch(logger,"GetDocumentTextDetectionResults Falied " + startJobId);
			}
		}
	}

	// Gets the results of processing started by StartDocumentTextDetection
	private static void GetDocumentTextDetectionResults(String jobId, LambdaLogger logger) throws Exception {
		int maxResults = 1000;
		String paginationToken = null;
		GetDocumentTextDetectionResult response = null;
		Boolean finished = false;
		linesText.clear();
		int pageNum = 1;
		while (finished == false) {
			Log2Cloudwatch(logger, "Processing Page: " + pageNum);
			
			GetDocumentTextDetectionRequest documentTextDetectionRequest = new GetDocumentTextDetectionRequest()
					.withJobId(jobId).withMaxResults(maxResults).withNextToken(paginationToken);
			response = textractClient.getDocumentTextDetection(documentTextDetectionRequest);
			DocumentMetadata documentMetaData = response.getDocumentMetadata();
			Log2Cloudwatch(logger, "Number of Pages: " + documentMetaData.getPages().toString());

			List<Block> blocks = response.getBlocks();
			for (Block block : blocks) {
				if ((block.getBlockType()).equals("LINE")) {
					linesText.add(block.getText());
				}
			}
			Log2Cloudwatch(logger, "Before processDetectedText, Number of lines of text:  " + linesText.size());
			processDetectedTextForPage(pageNum, linesText, logger);
			paginationToken = response.getNextToken();
			if (paginationToken == null) {
				finished = true;
				Log2Cloudwatch(logger, "Finished Processing Document: " + pageNum);
			}else {
				pageNum++;
			}
		}

	}

	private static void processDetectedTextForPage(int page, List<String> linesText,LambdaLogger logger) {
		Log2Cloudwatch(logger, String.format("***********************Text of Page :%s*******************************", page));
		for (int i = 0; i < linesText.size(); i++) {
			String lineOfText = linesText.get(i);
			Log2Cloudwatch(logger, lineOfText);
		}
	}

	public static boolean containsIgnoreCase(String src, String what) {
		final int srcLength = src.length();
		if (srcLength == 0) {
			return false; // Empty source
		}
		final int length = what.length();
		if (length == 0) {
			return true; // Empty string is contained
		}

		final char firstLo = Character.toLowerCase(what.charAt(0));
		final char firstUp = Character.toUpperCase(what.charAt(0));

		for (int i = src.length() - length; i >= 0; i--) {
			// Quick check before calling the more expensive regionMatches()
			final char ch = src.charAt(i);
			if (ch != firstLo && ch != firstUp) {
				continue;
			}

			if (src.regionMatches(true, i, what, 0, length)) {
				return true;
			}
		}

		return false;
	}

	private static boolean Log2Cloudwatch(LambdaLogger logger, String message) {
		try {
			if (bLog2Cloudwatch) {
				logger.log(message);
			} else {
				return false;
			}
		} catch (Exception logEx) {
			return false;
		}
		return true;
	}

}
