/**
 * 
 */
package com.clearpass.textract.async.get.results.sns;

/**
 * @author NZC26X
 *
 */
public class DocumentLocation {
	private String S3ObjectName;
	private String S3Bucket;

	// Getter Methods

	public String getS3ObjectName() {
		return S3ObjectName;
	}

	public String getS3Bucket() {
		return S3Bucket;
	}

	// Setter Methods

	public void setS3ObjectName(String S3ObjectName) {
		this.S3ObjectName = S3ObjectName;
	}

	public void setS3Bucket(String S3Bucket) {
		this.S3Bucket = S3Bucket;
	}
}