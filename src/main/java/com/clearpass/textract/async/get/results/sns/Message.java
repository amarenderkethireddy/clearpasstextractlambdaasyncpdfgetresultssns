/**
 * 
 */
package com.clearpass.textract.async.get.results.sns;

/**
 * @author NZC26X
 *
 */

public class Message {
	private String JobId;
	private String Status;
	private String API;
	private String JobTag;
	private float Timestamp;
	DocumentLocation DocumentLocationObject;

	// Getter Methods

	public String getJobId() {
		return JobId;
	}

	public String getStatus() {
		return Status;
	}

	public String getAPI() {
		return API;
	}

	public String getJobTag() {
		return JobTag;
	}

	public float getTimestamp() {
		return Timestamp;
	}

	public DocumentLocation getDocumentLocation() {
		return DocumentLocationObject;
	}

	// Setter Methods

	public void setJobId(String JobId) {
		this.JobId = JobId;
	}

	public void setStatus(String Status) {
		this.Status = Status;
	}

	public void setAPI(String API) {
		this.API = API;
	}

	public void setJobTag(String JobTag) {
		this.JobTag = JobTag;
	}

	public void setTimestamp(float Timestamp) {
		this.Timestamp = Timestamp;
	}

	public void setDocumentLocation(DocumentLocation DocumentLocationObject) {
		this.DocumentLocationObject = DocumentLocationObject;
	}
}