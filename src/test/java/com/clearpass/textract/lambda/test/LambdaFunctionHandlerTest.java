package com.clearpass.textract.lambda.test;

import java.io.IOException;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.events.S3Event;
import com.amazonaws.services.lambda.runtime.events.SQSEvent;
import com.amazonaws.services.lambda.runtime.events.SQSEvent.SQSMessage;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.sqs.AmazonSQS;
import com.clearpass.textract.async.get.results.sns.ClearpassTextractAsyncGetResultsLambdaHandlerSNS;

/**
 * A simple test harness for locally invoking your Lambda function handler.
 */
@RunWith(MockitoJUnitRunner.class)
public class LambdaFunctionHandlerTest {

    private final String CONTENT_TYPE = "image/jpeg";
    private SQSEvent event;
    
    private List<SQSEvent> events;

    @Mock
    private AmazonSQS sqsClient;
    @Mock
    private SQSMessage sqsMessageObject;


    @Before
    public void setUp() throws IOException {
        //event = TestUtils.parse("/sqs-event.put.json", SQSEvent.class);
    	//events = TestUtils.getSQSEventsList("/sqs-event.put.json");
          // TODO: customize your mock logic for s3 client
//        ObjectMetadata objectMetadata = new ObjectMetadata();
//        objectMetadata.setContentType(CONTENT_TYPE);
//        when(s3Object.getObjectMetadata()).thenReturn(objectMetadata);
//        when(sqsClient.getObject(getObjectRequest.capture())).thenReturn(s3Object);
    }

    private Context createContext() {
        TestContext ctx = new TestContext();

        // TODO: customize your context here if needed.
        ctx.setFunctionName("ProcessSQSRecord ");

        return ctx;
    }

    @Test
    public void testLambdaFunctionHandler() {
    	ClearpassTextractAsyncGetResultsLambdaHandlerSNS handler = new ClearpassTextractAsyncGetResultsLambdaHandlerSNS();
        Context ctx = createContext();
        
//        for(S3Event event : events) {
//        	 String output = handler.handleRequest(event, ctx);
//             Assert.assertEquals(CONTENT_TYPE, output);
//        }
       
    }
    
    
}
