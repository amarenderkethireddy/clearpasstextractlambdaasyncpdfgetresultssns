package com.clearpass.textract.lambda.test;

public class SQSEventRecords
{
    private Records[] Records;

    public Records[] getRecords ()
    {
        return Records;
    }

    public void setRecords (Records[] Records)
    {
        this.Records = Records;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [Records = "+Records+"]";
    }
}