package com.clearpass.textract.lambda.test;

import java.util.Iterator;
import java.util.List;

import com.amazonaws.services.lambda.runtime.events.SQSEvent;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Records implements Iterable<SQSEvent> {

    @Expose
    @SerializedName("Records")
    private List<SQSEvent> records;

    @Override
    public Iterator<SQSEvent> iterator() {
        return this.records.iterator();
    }

    public int size() {
        return this.records.size();
    }

    public void setRecords(List<SQSEvent> records) {
        this.records = records;
    }
}